"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request, url_for
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# DB Stuff
###

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/display")
def display():
    if db.brevets.count() == 1:
        b = db.brevets.find_one()
        return flask.render_template('display.html', distance=b['distance'], begin_date=b['begin_date'], checkpoints = b['checkpoints'])
    else:
        return 'No brevet is available for display'

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    start_iso = request.args.get('start_iso', arrow.now().isoformat(), type=str)
    brevet_dist = request.args.get('brevet_dist', 200, type=int)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist, start_iso)
    close_time = acp_times.close_time(km, brevet_dist, start_iso)
    result = {"open": open_time, "close": close_time}
    app.logger.debug(result)
    return flask.jsonify(result=result)

@app.route("/_submit", methods=['POST'])
def _submit():
    """
    Saves submitted brevet to mongodb database
    """
    msg = ""
    form = request.form.to_dict(flat=False)
    app.logger.info(form)
    distance, begin_date, begin_time = get_brevet_info(form)
    if distance and begin_date and begin_time:
        checkpoints = parse_checkpoints(form)
        if len(checkpoints):
            brevet = {'distance' : distance, 'begin_date' : begin_date, 'begin_time' : begin_time, 'checkpoints' : checkpoints}
            db.brevets.remove()
            db.brevets.insert_one(brevet)
            msg = "Brevet submitted successfully"
        else:
            msg = "You must create at least one checkpoint before submitting"
    else:
        "Incomplete information about the brevet was submitted."
    for b in db.brevets.find():
        print(b)
    return flask.jsonify(result={'msg':msg})

@app.route("/_display", methods=['GET'])
def _display():
    """ Gets a redirect to a display page if a brevet is saved in the DB """
    result = {}
    count = db.brevets.count({})
    if count == 1:
        redir = url_for('display')
        msg = 'Redirecting to brevet display.'
        result['redir'] = redir
        result['msg'] = msg
    else:
        msg = 'No brevet is currently saved'
        result['msg'] = msg
    return flask.jsonify(result=result)


###
# Helper Functions
###

def get_brevet_info(form):
    distance = form['distance']
    begin_date = form['begin_date']
    begin_time = form['begin_time']
    if distance and begin_date and begin_time:
        distance = int(distance[0])
        begin_date = begin_date[0]
        begin_time = begin_time[0]
    return distance, begin_date, begin_time


def parse_checkpoints(form):
    """ Extracts list of checkpoints from form, where each checkpoint is represented
    as a dict with keys miles, km, location, open, close """
    checkpoints = []
    miles = form['miles']
    km = form['km']
    location = form['location']
    open_times = form['open']
    close_times = form['close']
    for i in range(len(miles)):
        if miles[i] and km[i] and open_times[i] and close_times[i]:
            checkpoint = {}
            checkpoint['miles'] = miles[i]
            checkpoint['km'] = km[i]
            checkpoint['location'] = location[i]
            checkpoint['open'] = open_times[i]
            checkpoint['close'] = close_times[i]
            checkpoints.append(checkpoint)
    return checkpoints

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
