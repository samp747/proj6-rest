<html>
    <head>
        <title>Brevet Event Checkpoint Display</title>
    </head>

    <body>
        <h1>List of first 2 checkpoints</h1>
        <ul>
            <?php
            $json = file_get_contents('http://api-service/listAll?top=2');
            $obj = json_decode($json);
	          $checkpoints = $obj->checkpoints;
	    foreach ($checkpoints as $c) {
		    $km = $c->km;
		    $open = $c->open;
		    $close = $c->close;
                echo "<li><b>$km km Checkpoint:</b> Opens:$open Closes: $close</li>";
            }
            ?>
        </ul>
    </body>
</html>
