# Laptop Service

from flask import Flask, request, make_response
from flask_restful import Resource, Api, reqparse
import os
from pymongo import MongoClient

# Accessing DB
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetdb

# Instantiate the app
app = Flask(__name__)
api = Api(app)

def getAllCheckpoints():
        brevet = db.brevets.find_one()
        if brevet:
            checkpoints = brevet['checkpoints']
            return checkpoints
        return None

class AllJson(Resource):
    def get(self):
        top = request.args.get('top', default=None, type=int)
        checkpoints = getAllCheckpoints()
        # Removing extra info stored with each checkpoint in database
        if checkpoints:
            out = []
            checkpoints = sorted(checkpoints, key=lambda c: int(c['km']))
            if top is not None:
                checkpoints = checkpoints[:top]
            for c in checkpoints:
                small_c = {'km': c['km'], 'open':c['open'], 'close':c['close']} 
                out.append(small_c)
            return {'checkpoints':out}
        return {'checkpoints': []}

class AllCSV(Resource):
    def get(self):
        top = request.args.get('top', default=None, type=int)
        checkpoints = getAllCheckpoints()
        out = ""
        # Removing extra info stored with each checkpoint in database
        if checkpoints:
            checkpoints = sorted(checkpoints, key=lambda c: int(c['km']))
            if top is not None:
                checkpoints = checkpoints[:top]
            for c in checkpoints:
                line = ",".join([c['km'], c['open'], c['close']]) + "\n"
                out += line
            # Removing final newline character
            out = out[:-1]
        resp = make_response(out)
        resp.headers['content-type'] = 'application/csv'
        return resp

class OpenJson(Resource):
    def get(self):
        top = request.args.get('top', default=None, type=int)
        checkpoints = getAllCheckpoints()
        # Removing extra info stored with each checkpoint in database
        if checkpoints:
            out = []
            checkpoints = sorted(checkpoints, key=lambda c: int(c['km']))
            if top is not None:
                checkpoints = checkpoints[:top]
            for c in checkpoints:
                small_c = {'km': c['km'], 'open':c['open']} 
                out.append(small_c)
            return {'checkpoints':out}
        return {'checkpoints': []}

class OpenCSV(Resource):
    def get(self):
        top = request.args.get('top', default=None, type=int)
        checkpoints = getAllCheckpoints()
        out = ""
        # Removing extra info stored with each checkpoint in database
        if checkpoints:
            checkpoints = sorted(checkpoints, key=lambda c: int(c['km']))
            if top is not None:
                checkpoints = checkpoints[:top]
            for c in checkpoints:
                line = ",".join([c['km'], c['open']]) + "\n"
                out += line
            # Removing final newline character
            out = out[:-1]
        resp = make_response(out)
        resp.headers['content-type'] = 'application/csv'
        return resp

class CloseJson(Resource):
    def get(self):
        top = request.args.get('top', default=None, type=int)
        checkpoints = getAllCheckpoints()
        # Removing extra info stored with each checkpoint in database
        if checkpoints:
            out = []
            checkpoints = sorted(checkpoints, key=lambda c: int(c['km']))
            if top is not None:
                checkpoints = checkpoints[:top]
            for c in checkpoints:
                small_c = {'km': c['km'], 'close':c['close']} 
                out.append(small_c)
            return {'checkpoints':out}
        return {'checkpoints': []}

class CloseCSV(Resource):
    def get(self):
        top = request.args.get('top', default=None, type=int)
        checkpoints = getAllCheckpoints()
        out = ""
        # Removing extra info stored with each checkpoint in database
        if checkpoints:
            checkpoints = sorted(checkpoints, key=lambda c: int(c['km']))
            if top is not None:
                checkpoints = checkpoints[:top]
            for c in checkpoints:
                line = ",".join([c['km'], c['close']]) + "\n"
                out += line
            # Removing final newline character
            out = out[:-1]
        resp = make_response(out)
        resp.headers['content-type'] = 'application/csv'
        return resp

# Create routes
# Another way, without decorators
api.add_resource(AllJson, '/listAll/json', endpoint='get_all_json')
api.add_resource(AllCSV, '/listAll/csv', endpoint='get_all_csv')
api.add_resource(AllJson, '/listAll')
api.add_resource(OpenJson, '/listOpenOnly/json', endpoint='get_open_json')
api.add_resource(OpenCSV, '/listOpenOnly/csv', endpoint='get_open_csv')
api.add_resource(OpenJson, '/listOpenOnly')
api.add_resource(CloseJson, '/listCloseOnly/json', endpoint='get_close_json')
api.add_resource(CloseCSV, '/listCloseOnly/csv', endpoint='get_close_csv')
api.add_resource(CloseJson, '/listCloseOnly')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
